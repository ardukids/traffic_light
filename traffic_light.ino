int pinRed = 13;
int pinYel = 12;
int pinGre = 11;

void setup() 
{
  pinMode(pinRed, OUTPUT);
  pinMode(pinYel, OUTPUT);
  pinMode(pinGre, OUTPUT);
}

void loop() 
{
  //Red light
  digitalWrite(pinRed, HIGH);
  delay(5000);
  digitalWrite(pinRed, LOW);

  //Yellow light 
  digitalWrite(pinYel, HIGH);
  delay(3000);
  digitalWrite(pinYel, LOW);

  //Green light
  digitalWrite(pinGre, HIGH);
  delay(5000);
  digitalWrite(pinGre, LOW);
}
